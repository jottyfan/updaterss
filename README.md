# updaterss

a simple shell script that sums update candidates and list them in a RSS stream

## build

`fakeroot dpkg -b updaterss` will build the project as a deb file. Name it at your will.

## setup and install

Install the package by `sudo dpkg -i PACKAGE_NAME.deb`, where `PACKAGE_NAME` is the name that you have defined on building it.

# Instructions

It is a good idea to secure that information by using a restricted access to the RSS file. Therefore, consider to use https restricted areas.
