#!/bin/bash

F=$(more /etc/updaterss/updaterss.conf | grep "rssfile" | sed -e "s/rssfile=//g")
T=$(more /etc/updaterss/updaterss.conf | grep "title" | sed -e "s/title=//g")
L=$(more /etc/updaterss/updaterss.conf | grep "link" | sed -e "s/link=//g")
D=$(more /etc/updaterss/updaterss.conf | grep "description" | sed -e "s/description=//g")
H=$(date +"%a, %d %b %Y %H:%M:%S %z")
N=$(date +%Y%M%d%s)

apt update
U=$(aptitude search -F '%p [&lt;b&gt;%v&lt;/b&gt;] ➜ &lt;b&gt;%V&lt;/b&gt;&lt;br&gt;' --disable-columns '~U')

echo "$U"

echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $F
echo "<rss version=\"2.0\">" >> $F
echo "<channel>" >> $F
echo "<title>$T</title>" >> $F
echo "<link>$L</link>" >> $F
echo "<description>$D</description>" >> $F
echo "<language>en-us</language>" >> $F
echo "<pubDate>$H</pubDate>" >> $F
if [ "$U" != "" ]
then
  echo "<item>" >> $F
  echo "<title>Updates</title>" >> $F
  echo "<link>$L</link>" >> $F
  echo "<description>$U</description>" >> $F
  echo "<pubDate>$H</pubDate>" >> $F
  echo "<guid>$L#$N</guid>" >> $F
  echo "</item>" >> $F
else
  echo "<item>" >> $F
  echo "<title>no updates</title>" >> $F
  echo "<link>$L</link>" >> $F
  echo "<description>no updates found</description>" >> $F
  echo "</item>" >> $F
fi
echo "</channel>" >> $F
echo "</rss>" >> $F
